<?php

/**
 * @file
 * uw_cfg_publication.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_cfg_publication_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cover_photo|taxonomy_term|publication_issue|form';
  $field_group->group_name = 'group_cover_photo';
  $field_group->entity_type = 'taxonomy_term';
  $field_group->bundle = 'publication_issue';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Cover Photo',
    'weight' => '5',
    'children' => array(
      0 => 'field_cover_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cover-photo field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_cover_photo|taxonomy_term|publication_issue|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_images|node|nested_publication_articles|form';
  $field_group->group_name = 'group_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'nested_publication_articles';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload Image',
    'weight' => '7',
    'children' => array(
      0 => 'field_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-images field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_images|node|nested_publication_articles|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_images|node|publication_article|form';
  $field_group->group_name = 'group_images';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'publication_article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload Images',
    'weight' => '9',
    'children' => array(
      0 => 'field_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload Images',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_images|node|publication_article|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_order|node|publication_article|form';
  $field_group->group_name = 'group_order';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'publication_article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Order',
    'weight' => '25',
    'children' => array(
      0 => 'field_order',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-order field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_order|node|publication_article|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pdf|taxonomy_term|publication_issue|form';
  $field_group->group_name = 'group_pdf';
  $field_group->entity_type = 'taxonomy_term';
  $field_group->bundle = 'publication_issue';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'PDF',
    'weight' => '6',
    'children' => array(
      0 => 'field_file',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-pdf field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_pdf|taxonomy_term|publication_issue|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload_files|node|nested_publication_articles|form';
  $field_group->group_name = 'group_upload_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'nested_publication_articles';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload Files',
    'weight' => '8',
    'children' => array(
      0 => 'field_file',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-upload-files field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_upload_files|node|nested_publication_articles|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_upload_files|node|publication_article|form';
  $field_group->group_name = 'group_upload_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'publication_article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Upload Files',
    'weight' => '10',
    'children' => array(
      0 => 'field_file',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Upload Files',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_upload_files|node|publication_article|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_pub_listing_image|node|publication_article|form';
  $field_group->group_name = 'group_uw_pub_listing_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'publication_article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Listing page image',
    'weight' => '4',
    'children' => array(
      0 => 'field_feature_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-uw-pub-listing-image field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_uw_pub_listing_image|node|publication_article|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_pub_sub_nav_icon|node|publication_article|form';
  $field_group->group_name = 'group_uw_pub_sub_nav_icon';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'publication_article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sub-navigation icon',
    'weight' => '5',
    'children' => array(
      0 => 'field_uw_pub_sub_nav_icon_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-uw-pub-sub-nav-icon field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_uw_pub_sub_nav_icon|node|publication_article|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Cover Photo');
  t('Listing page image');
  t('Order');
  t('PDF');
  t('Sub-navigation icon');
  t('Upload Files');
  t('Upload Image');
  t('Upload Images');

  return $field_groups;
}
