<?php

/**
 * @file
 * uw_cfg_publication.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function uw_cfg_publication_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-footer-menu.
  $menus['menu-footer-menu'] = array(
    'menu_name' => 'menu-footer-menu',
    'title' => 'Footer Menu',
    'description' => 'Footer menu for publication',
    'language' => 'und',
    'i18n_mode' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Footer Menu');
  t('Footer menu for publication');

  return $menus;
}
