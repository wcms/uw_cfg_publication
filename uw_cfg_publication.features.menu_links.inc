<?php

/**
 * @file
 * uw_cfg_publication.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_cfg_publication_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-footer-menu_about:node/2.
  $menu_links['menu-footer-menu_about:node/2'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'node/2',
    'router_path' => 'node/%',
    'link_title' => 'About',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_about:node/2',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-footer-menu_copyright:https://uwaterloo.ca/copyright.
  $menu_links['menu-footer-menu_copyright:https://uwaterloo.ca/copyright'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'https://uwaterloo.ca/copyright',
    'router_path' => '',
    'link_title' => 'Copyright',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_copyright:https://uwaterloo.ca/copyright',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-footer-menu_privacy-policy:https://uwaterloo.ca/privacy.
  $menu_links['menu-footer-menu_privacy-policy:https://uwaterloo.ca/privacy'] = array(
    'menu_name' => 'menu-footer-menu',
    'link_path' => 'https://uwaterloo.ca/privacy',
    'router_path' => '',
    'link_title' => 'Privacy Policy',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-footer-menu_privacy-policy:https://uwaterloo.ca/privacy',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_publication-categories:admin/structure/taxonomy/publication_categories.
  $menu_links['menu-site-manager-vocabularies_publication-categories:admin/structure/taxonomy/publication_categories'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/publication_categories',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Publication Categories',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_publication-categories:admin/structure/taxonomy/publication_categories',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_publication-issue:admin/structure/taxonomy/publication_issue.
  $menu_links['menu-site-manager-vocabularies_publication-issue:admin/structure/taxonomy/publication_issue'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/publication_issue',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Publication Issue',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_publication-issue:admin/structure/taxonomy/publication_issue',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_publication-types:admin/structure/taxonomy/publication_type.
  $menu_links['menu-site-manager-vocabularies_publication-types:admin/structure/taxonomy/publication_type'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/publication_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Publication Types',
    'options' => array(
      'attributes' => array(
        'title' => 'Edit the \'content contains\' type you can tag publication with.',
      ),
      'identifier' => 'menu-site-manager-vocabularies_publication-types:admin/structure/taxonomy/publication_type',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-site-manager-vocabularies_publication-volume:admin/structure/taxonomy/publication_volume.
  $menu_links['menu-site-manager-vocabularies_publication-volume:admin/structure/taxonomy/publication_volume'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/publication_volume',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Publication Volume',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_publication-volume:admin/structure/taxonomy/publication_volume',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('About');
  t('Copyright');
  t('Privacy Policy');
  t('Publication Categories');
  t('Publication Issue');
  t('Publication Types');
  t('Publication Volume');

  return $menu_links;
}
