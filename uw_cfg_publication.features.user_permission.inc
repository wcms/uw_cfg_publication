<?php

/**
 * @file
 * uw_cfg_publication.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_publication_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'add terms in publication_categories'.
  $permissions['add terms in publication_categories'] = array(
    'name' => 'add terms in publication_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy_access_fix',
  );

  // Exported permission: 'add terms in publication_issue'.
  $permissions['add terms in publication_issue'] = array(
    'name' => 'add terms in publication_issue',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy_access_fix',
  );

  // Exported permission: 'add terms in publication_type'.
  $permissions['add terms in publication_type'] = array(
    'name' => 'add terms in publication_type',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy_access_fix',
  );

  // Exported permission: 'add terms in publication_volume'.
  $permissions['add terms in publication_volume'] = array(
    'name' => 'add terms in publication_volume',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy_access_fix',
  );

  // Exported permission: 'administer menu-footer-menu menu items'.
  $permissions['administer menu-footer-menu menu items'] = array(
    'name' => 'administer menu-footer-menu menu items',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'award content author' => 'award content author',
      'events editor' => 'events editor',
      'site manager' => 'site manager',
    ),
    'module' => 'menu_admin_per_menu',
  );

  // Exported permission: 'administer uw_cfg_publication settings'.
  $permissions['administer uw_cfg_publication settings'] = array(
    'name' => 'administer uw_cfg_publication settings',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_cfg_publication',
  );

  // Exported permission: 'change publication theme'.
  $permissions['change publication theme'] = array(
    'name' => 'change publication theme',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'uw_cfg_publication',
  );

  // Exported permission: 'create nested_publication_articles content'.
  $permissions['create nested_publication_articles content'] = array(
    'name' => 'create nested_publication_articles content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create publication_article content'.
  $permissions['create publication_article content'] = array(
    'name' => 'create publication_article content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any nested_publication_articles content'.
  $permissions['delete any nested_publication_articles content'] = array(
    'name' => 'delete any nested_publication_articles content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any publication_article content'.
  $permissions['delete any publication_article content'] = array(
    'name' => 'delete any publication_article content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own nested_publication_articles content'.
  $permissions['delete own nested_publication_articles content'] = array(
    'name' => 'delete own nested_publication_articles content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own publication_article content'.
  $permissions['delete own publication_article content'] = array(
    'name' => 'delete own publication_article content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in publication_categories'.
  $permissions['delete terms in publication_categories'] = array(
    'name' => 'delete terms in publication_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in publication_issue'.
  $permissions['delete terms in publication_issue'] = array(
    'name' => 'delete terms in publication_issue',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in publication_type'.
  $permissions['delete terms in publication_type'] = array(
    'name' => 'delete terms in publication_type',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in publication_volume'.
  $permissions['delete terms in publication_volume'] = array(
    'name' => 'delete terms in publication_volume',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any nested_publication_articles content'.
  $permissions['edit any nested_publication_articles content'] = array(
    'name' => 'edit any nested_publication_articles content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any publication_article content'.
  $permissions['edit any publication_article content'] = array(
    'name' => 'edit any publication_article content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own nested_publication_articles content'.
  $permissions['edit own nested_publication_articles content'] = array(
    'name' => 'edit own nested_publication_articles content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own publication_article content'.
  $permissions['edit own publication_article content'] = array(
    'name' => 'edit own publication_article content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in publication_categories'.
  $permissions['edit terms in publication_categories'] = array(
    'name' => 'edit terms in publication_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in publication_issue'.
  $permissions['edit terms in publication_issue'] = array(
    'name' => 'edit terms in publication_issue',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in publication_type'.
  $permissions['edit terms in publication_type'] = array(
    'name' => 'edit terms in publication_type',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in publication_volume'.
  $permissions['edit terms in publication_volume'] = array(
    'name' => 'edit terms in publication_volume',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter nested_publication_articles revision log entry'.
  $permissions['enter nested_publication_articles revision log entry'] = array(
    'name' => 'enter nested_publication_articles revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter publication_article revision log entry'.
  $permissions['enter publication_article revision log entry'] = array(
    'name' => 'enter publication_article revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override nested_publication_articles authored by option'.
  $permissions['override nested_publication_articles authored by option'] = array(
    'name' => 'override nested_publication_articles authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override nested_publication_articles authored on option'.
  $permissions['override nested_publication_articles authored on option'] = array(
    'name' => 'override nested_publication_articles authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override nested_publication_articles promote to front page option'.
  $permissions['override nested_publication_articles promote to front page option'] = array(
    'name' => 'override nested_publication_articles promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override nested_publication_articles published option'.
  $permissions['override nested_publication_articles published option'] = array(
    'name' => 'override nested_publication_articles published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override nested_publication_articles revision option'.
  $permissions['override nested_publication_articles revision option'] = array(
    'name' => 'override nested_publication_articles revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override nested_publication_articles sticky option'.
  $permissions['override nested_publication_articles sticky option'] = array(
    'name' => 'override nested_publication_articles sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override publication_article authored by option'.
  $permissions['override publication_article authored by option'] = array(
    'name' => 'override publication_article authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override publication_article authored on option'.
  $permissions['override publication_article authored on option'] = array(
    'name' => 'override publication_article authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override publication_article promote to front page option'.
  $permissions['override publication_article promote to front page option'] = array(
    'name' => 'override publication_article promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override publication_article published option'.
  $permissions['override publication_article published option'] = array(
    'name' => 'override publication_article published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override publication_article revision option'.
  $permissions['override publication_article revision option'] = array(
    'name' => 'override publication_article revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override publication_article sticky option'.
  $permissions['override publication_article sticky option'] = array(
    'name' => 'override publication_article sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'publication meta-data'.
  $permissions['publication meta-data'] = array(
    'name' => 'publication meta-data',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'uw_restful_publication',
  );

  // Exported permission: 'set taxonomy menu'.
  $permissions['set taxonomy menu'] = array(
    'name' => 'set taxonomy menu',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy_menu_form',
  );

  return $permissions;
}
