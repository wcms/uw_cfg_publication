/**
 * @file
 * Use jQuery to provide summary information inside vertical tabs.
 */

(function ($) {

  /**
   * Provide summary information for vertical tabs.
   */
  Drupal.behaviors.publication_article_node_edit = {
    attach: function (context) {

      // Provide summary when editing a node.
      $('fieldset#edit-group_order', context).drupalSetSummary(function (context) {
        order = $('#edit-field-order-und-0-value').val();
        if (order == '') {
          order = 'blank';
        }
        return 'Current order: ' + order;
      });

    }
  };

})(jQuery);
