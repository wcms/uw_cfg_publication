<?php

/**
 * @file
 * uw_cfg_publication.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function uw_cfg_publication_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:publication_article.
  $config['node:publication_article'] = array(
    'instance' => 'node:publication_article',
    'config' => array(
      'description' => array(
        'value' => '[node:field_publication_teaser]',
      ),
      'og:description' => array(
        'value' => '[node:field_publication_teaser]',
      ),
      'og:image' => array(
        'value' => '[node:field_feature_image]',
      ),
      'article:section' => array(
        'value' => '[node:field-publication-category]',
      ),
      'twitter:description' => array(
        'value' => '[node:field_publication_teaser]',
      ),
      'twitter:image' => array(
        'value' => '[node:field_feature_image]',
      ),
    ),
  );

  return $config;
}
