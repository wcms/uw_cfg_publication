<?php

/**
 * @file
 * uw_cfg_publication.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_cfg_publication_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_cfg_publication_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function uw_cfg_publication_image_default_styles() {
  $styles = array();

  // Exported image style: cover_photo.
  $styles['cover_photo'] = array(
    'label' => 'cover photo',
    'effects' => array(
      5 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 1000,
          'height' => 225,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: cover_photo_x2.
  $styles['cover_photo_x2'] = array(
    'label' => 'cover photo x2',
    'effects' => array(
      6 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 2000,
          'height' => 450,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: maxwidth1000.
  $styles['maxwidth1000'] = array(
    'label' => 'maxwidth1000',
    'effects' => array(
      7 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1000,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: original.
  $styles['original'] = array(
    'label' => 'original',
    'effects' => array(),
  );

  // Exported image style: size540x370.
  $styles['size540x370'] = array(
    'label' => '540x370',
    'effects' => array(
      2 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 540,
          'height' => 370,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: width300.
  $styles['width300'] = array(
    'label' => 'width300',
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 300,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_cfg_publication_node_info() {
  $items = array(
    'nested_publication_articles' => array(
      'name' => t('Nested Publication Articles'),
      'base' => 'node_content',
      'description' => t('An article that lives inside the main article.  The article is too small or short to be a main article. It has less feature and requirements than the main publication article.'),
      'has_title' => '1',
      'title_label' => t('Headline'),
      'help' => '',
    ),
    'publication_article' => array(
      'name' => t('Publication Article'),
      'base' => 'node_content',
      'description' => t('An article for a publication website.  This will be attached to an issue and/or volume'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
