<?php

/**
 * @file
 * uw_cfg_publication.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_cfg_publication_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Feature',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'machine_name' => 'feature',
    'uuid' => '994b4826-7c3b-487a-b4f9-e5998c0101c3',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'publication_categories',
    'description_field' => array(
      'und' => array(
        0 => array(
          'value' => '',
          'summary' => NULL,
          'format' => 'uw_tf_standard',
          'safe_summary' => '',
        ),
      ),
    ),
    'name_field' => array(
      'en' => array(
        0 => array(
          'value' => 'Feature',
          'format' => NULL,
        ),
      ),
      'und' => array(
        0 => array(
          'value' => 'Feature',
          'format' => NULL,
        ),
      ),
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'entity_translation_handler_id' => 'taxonomy_term-publication_categories-eid-45-0',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
